import React, { FC, useState, useEffect, useCallback } from "react";
import _ from 'lodash'

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();
const DEFAULT_USERS_AMOUNT = 20

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [data, setData] = useState<{
    data: IUserProps[],
    hasMore: boolean,
    totalCount: number
  }
  >({
    data: [],
    hasMore: false,
    totalCount: 0
  });
  const [loading, setLoading] = useState<boolean>(false)
  const [searchBy, setSearchBy] = useState<string>('')
  const [page, setPage] = useState<number>(1)

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const result = await backendClient.getAllUsers({ limit: DEFAULT_USERS_AMOUNT, offset: DEFAULT_USERS_AMOUNT * (page - 1), searchBy });

        setData(result);
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [page, searchBy]);

  useEffect(() => {
    setPage(1)
  }, [searchBy])

  const handleChangeSearchBy = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    setSearchBy(e.target.value)
  };

  const handleNextPage = (pageNumber: number) => {
    setPage(pageNumber)
  }

  const pageAmount = new Array(Math.ceil(data.totalCount / DEFAULT_USERS_AMOUNT) || 0).fill(1)
  console.log('data.totalAmount: ', data.totalCount);
  console.log('pageAmount: ', pageAmount);

  return (
    <div style={{ paddingTop: "30px" }}>
      <div>
        <input
          placeholder="Search...."
          value={searchBy}
          onChange={handleChangeSearchBy}
        />
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {data.data.length
              ? data.data.map((user) => {
                return <UserCard key={user.id} {...user} />;
              })
              : null}
          </div>
        )}
      </div>
      <div>
        <h5>Pagination</h5>
        <p>Page {page}</p>
        <div
        style={{
          display: 'flex',
          flexDirection: 'row'
        }}
        >
        {pageAmount.map((item, index) => {
          const handleClick = () => {
            handleNextPage(index + 1)
          }
          return <p onClick={handleClick} key={`page_${index}`}>{index + 1}</p>
        })}
        </div>
        <button >next page</button>
      </div>
    </div>
  );
};
