import people_data from "../data/people_data.json";
import _ from "lodash";

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll(params: { offset: number; limit?: number; searchBy?: string }) {
    const response = _.chain(people_data)
      .filter((item) => {
        const searchStr = (
          item.first_name +
          item.last_name +
          item.title
        ).toLowerCase();
        return _.includes(
          searchStr,
          params.searchBy ? params.searchBy.toLowerCase() : ""
        );
      })
      .slice(
        +params.offset,
        +params.offset +
          (params?.limit ? +params?.limit : people_data?.length - 1)
      )
      .value();

    console.log("response: ", response?.length);

    return response;
  }
}
