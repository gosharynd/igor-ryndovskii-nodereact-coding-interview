import axios from "axios";
import { IUserProps } from "../dtos/user.dto";

const DEFAULT_USERS_AMOUNT = 40;

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers({
    searchBy,
    offset = 0,
    limit = DEFAULT_USERS_AMOUNT,
  }: {
    searchBy?: string;
    offset?: number;
    limit?: number;
  }): Promise<{
    data: IUserProps[];
    hasMore: boolean;
    totalCount: number;
  }> {
    return (
      await axios.get(`${this.baseUrl}/people/all`, {
        params: {
          searchBy,
          limit,
          offset,
        },
      })
    ).data;
  }
}
